# Workshop 3: Collaborating with Python

This is the third in a series of scientific Python lectures at Risø campus,
Denmark Technical University.

## Workshop background

**Objective**

To make users more comfortable with packaging and GitLab so that they can more
easily share their code and collaborate with others.

**Who should come**

Anyone interested in writing code collaboratively. Both new and experienced 
Python users are welcome.

**Date**

The workshop date and location will be announced internally at DTU Risø. Please
use the contact information below for questions on the workshop contents or 
arranging a new workshop.

## Topic outline

- Creating a package
    - Structuring a module
    - Installing with `setup.py` and pip
- Git/GitHub/GitLab
    - What is git and why it's useful to scientists/engineers
    - Git "quickstart"
    - How to share your package using GitLab

## Before the workshop

If you are attending the workshop, please do the following before attending:
1. If you do not have Anaconda installed, please [install it](https://www.anaconda.com/download/)
**with Python 3.6**
2. If you have Anaconda installed, please either  
    a) have your root environment be Python 3.6, or  
    b) [create an environment](https://conda.io/docs/user-guide/tasks/manage-environments.html#creating-an-environment-with-commands)
    that has Python 3.6
3. If you don't have a git client, install one. I use [git for Windows](https://git-scm.com/download/win).
4. If you haven't signed up on GitLab, do so.
5. Clone this repository, and make sure that you can `git pull` without issue
(see more detailed instructions in [Workshop 2](https://gitlab.windenergy.dtu.dk/python-at-risoe/scientific-python-workshops/2-getting-started)).
6. Also clone [this repository](https://gitlab.windenergy.dtu.dk/python-at-risoe/my-cool-package)
to your machine.


## Contact

Jenni Rinker  
rink@dtu.dk