# Further Resources

### 1. Git and GitHub/GitLab

The workshop will not provide a very detailed overview of what git/GitLab can
do and their capabilities. Here is a quickly compiled list of other places
you can learn more about git and how to use it.

**Git for scientists**

- [Git tutorial for scientists](http://nyuccl.org/pages/gittutorial/)
- [Why you need git](https://blog.okfn.org/2016/11/29/git-for-data-analysis-why-version-control-is-essential-collaboration-public-trust/)

**Video Tutorials**

I (Jenni) have put a few notes in the video tutorials on my impressions after
skimming the videos. Take my "reviews" with a grain of salt.

- [Codemy School](https://www.youtube.com/channel/UCXNeUcG4KpZaYUgYJvgN9tg):
Fairly engaging. I would recommend these.
    - [Intro to git and GitHub](https://www.youtube.com/watch?v=uUuTYDg9XoI)
    - [First steps with git](https://www.youtube.com/watch?v=QqP7YZlZEOo)
    - [Working with branches](https://www.youtube.com/watch?v=JTE2Fn_sCZs)
- [Packt Videos](https://www.youtube.com/channel/UC3VydBGBl132baPCLeDspMQ):
Slightly dry, but good content and follow-alongs
    - [Learning git playlist](https://www.youtube.com/watch?v=NW46XmvJh5Q&list=PLTgRMOcmRb3NeFdWsbv8dOMmCSTcc71W0)
    - [Mastering git playlist](https://www.youtube.com/watch?v=clkDENcltN4&list=PLTgRMOcmRb3MdUwwF0j-bpFZuJt0kpmgp)
- [Tutorials from git themselves](https://git-scm.com/videos): I found these to
be a little boring

**Written References**

- [My favorite git cheatsheet](https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet)
    - A good reference for quickly looking up when you forget a command
- [Git book](https://git-scm.com/book/en/v2)
    - This is free and available online
- [A git repo listing a bunch of references](https://gist.github.com/jaseemabid/1321592)
    - Seems detailed. I haven't looked through them all.
- [Git from the bottom up](https://jwiegley.github.io/git-from-the-bottom-up/)
    - Perhaps not good for total newbs, but excellent if you have some
    experience
- [Easy guide (with GitHub!)](https://code.tutsplus.com/tutorials/easy-version-control-with-git--net-7449)
    - I found this to be a nice reference, and it's one of the few that also
    discusses GitHub
- [Follow-along tutorial at Atlassian](https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud)
   - Clearly written, but you need a bitbucket account
- [Collaborating with GitHub](http://www.eqqon.com/index.php/Collaborative_Github_Workflow)
    - Nice overview of forking, branching, merging, etc.
- [GitHub vs. GitLab](https://usersnap.com/blog/gitlab-github/)
    - A good explanation of the difference between GitHub and GitLab, with some
    talks on git as well

### 2. Packaging

- [Quickstart guide](http://the-hitchhikers-guide-to-packaging.readthedocs.io/en/latest/quickstart.html)
    - An excellent get-off-the-ground tutorial
- [An example PyPi project](https://pythonhosted.org/an_example_pypi_project/setuptools.html)
    - Some more nice details about the setup file